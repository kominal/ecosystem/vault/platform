#!/bin/bash
docker run --rm --network infrastructure_cluster mongo:latest bash -c " \
    mongo --host kominal/mongodb01,mongodb02,mongodb03 ${STACK_NAME} --authenticationDatabase admin -u admin -p ${DATABASE_ADMIN_PASSWORD} --eval \"db.createUser({ user:'vault-service', pwd:'${DATABASE_PASSWORD_VAULT_SERVICE}', roles:[{ role:'readWrite', db:'${STACK_NAME}_vault-service' }] })\"; \
"